const wrapper = document.querySelector('.wrapper');
const loader = document.getElementById('loader');
let ready = false;
let imagesLoaded = 0;
let totalImages = 0;
let photosArray = [];


function imageLoaded() {
  imagesLoaded++;
  if (imagesLoaded === totalImages) {
    ready = true;
    loader.hidden = true;
  }
}

function setAttributes(element, attributes) {
  for (const key in attributes) {
    element.setAttribute(key, attributes[key]);
  }
}

function displayPhotos() {
  imagesLoaded = 0;
  totalImages = photosArray.length;
  photosArray.forEach((photo) => {
    const anchor = document.createElement('a');
    setAttributes(anchor, {
      href: photo.links.html,
      target: '_blank',
    });

    const image = document.createElement('img');
    setAttributes(image, {
      src: photo.urls.regular,
      alt: photo.alt_description,
      title: photo.alt_description,
    });
    image.addEventListener('load', imageLoaded);

    anchor.appendChild(image);
    wrapper.appendChild(anchor);
  });
}

async function getPhotos() {
  const base_url = 'https://api.unsplash.com/';
  const $ACCESS_KEY = 'F7c8FOsY9wmPGyCGL9pikqV8iEO0tQPecKX4OyDcX0A';
  try {
    const api_call = await fetch(
      `${base_url}photos/random/?client_id=${$ACCESS_KEY}&count=30`
    );
    photosArray = await api_call.json();
    console.log(photosArray);
    displayPhotos();
  } catch (err) {
    console.log(err);
  }
}


window.addEventListener('scroll', () => {
  if (
    window.innerHeight + window.scrollY >= document.body.offsetHeight - 1000 &&
    ready
  ) {
    ready = false;
    getPhotos();
  }
});

getPhotos();